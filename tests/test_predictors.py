import os
import unittest

from logistic_predictor import CSVLogisticPredictor, PredictionException


TEST_OUTPUT_FILEPATH = "test_data/a_file.csv"
class TestLogisticPredictor(unittest.TestCase):
    def setUp(self):
        if os.path.exists(TEST_OUTPUT_FILEPATH):
            # removing file used on the test
            os.remove(TEST_OUTPUT_FILEPATH)


    def test_index_column_is_used(self):
        """
            Checking if the column passed on the constructor is used on
            the dataFrame index
        """
        # No index_column passed, first column of the csv file should be used
        predictor = CSVLogisticPredictor("test_data/example.csv", "test_data/example.csv")
        self.assertEquals(predictor.train_data.index[0], "like new", "should use csv first value of first row")
        self.assertEquals(predictor.test_data.index[0], "like new", "should use csv first value of first row")
        self.assertTrue("UniqueID" in predictor.test_data.columns)
        self.assertTrue("UniqueID" in predictor.train_data.columns)
        
        # Index column passed, it should be used and should be removed from the columns
        predictor = CSVLogisticPredictor("test_data/example.csv", "test_data/example.csv", "UniqueID")
        self.assertEquals(predictor.train_data.index[0], 11862, "should use csv 'UniqueID' value of first row")
        self.assertEquals(predictor.train_data.index[0], 11862, "should use csv 'UniqueID' value of first row")
        self.assertFalse("UniqueID" in predictor.test_data.columns)
        self.assertFalse("UniqueID" in predictor.train_data.columns)


    def test_categorical_columns_creation_and_removal(self):
        """
            Checking if the source column is removed and if the
            categorical values of the source column will generate
            new columns.
        """
        predictor = CSVLogisticPredictor("test_data/example.csv", 
                                     "test_data/example.csv", 
                                     "UniqueID")
        categorical_column = 'condition'
        
        categorical_values = predictor.train_data[categorical_column].unique()
        
        test_columns_before_preparation = predictor.test_data.columns
        train_columns_before_preparation = predictor.test_data.columns
        self.assertTrue(categorical_column in test_columns_before_preparation)
        self.assertTrue(categorical_column in train_columns_before_preparation)

        #################################
        # PREPARING CATEGORICAL COLUMNS
        #################################
        predictor.prepare_categorical_columns([categorical_column])
        test_columns_after_preparation = predictor.test_data.columns
        train_columns_after_preparation = predictor.test_data.columns
        
        diff_test_columns = len(test_columns_after_preparation) - len(test_columns_before_preparation)
        diff_train_columns = len(test_columns_after_preparation) - len(test_columns_before_preparation)
        
        # first column and source column should be removed
        self.assertEquals(diff_test_columns, len(categorical_values)-2)
        self.assertEquals(diff_train_columns, len(categorical_values)-2) 
        
        # categorical column should be removed
        self.assertFalse(categorical_column in test_columns_after_preparation)
        self.assertFalse(categorical_column in train_columns_after_preparation)
        
        # new columns should be created
        for categorical_value in categorical_values[1:]: # first column is not created
            new_column_name = "cat_%s_%s" % (categorical_column, categorical_value.replace(" ",""))
            self.assertTrue(new_column_name in test_columns_after_preparation)
            self.assertTrue(new_column_name in train_columns_after_preparation)
    
    
    def test_categorical_columns_values(self):
        """
            Checking if the prepare_categorical_columns method is creating
            the categorical columns and if it is putting the right value
            inside each column
        """
        categorical_values_original = ['white', 'red', 'blue', 'pink']
        original_values = ['white', 'white', 'red', 'blue', 'red', 'pink', 'pink']
        
        predictor = CSVLogisticPredictor("test_data/simple_example.csv", 
                                     "test_data/simple_example.csv")
        categorical_column = 'color'
        
        categorical_values = predictor.train_data[categorical_column].unique()
        self.assertEquals(categorical_values.tolist(), categorical_values_original)
        
        self.assertTrue(categorical_column in predictor.test_data.columns)
        self.assertTrue(categorical_column in predictor.train_data.columns)

        #################################
        # PREPARING CATEGORICAL COLUMNS
        #################################
        created_column_names = predictor.prepare_categorical_columns([categorical_column])
        self.assertFalse(categorical_column in predictor.test_data.columns)
        self.assertFalse(categorical_column in predictor.train_data.columns)

        # new columns should be created
        for i, categorical_value in enumerate(categorical_values_original):
            new_column_name = "cat_color_%s" % (categorical_value)
            if i==0:
                # first column is not used
                self.assertFalse(new_column_name in predictor.test_data.columns)
                self.assertFalse(new_column_name in predictor.test_data.columns)
            else:
                self.assertTrue(new_column_name in predictor.test_data.columns)
                self.assertTrue(new_column_name in predictor.test_data.columns)
        
        # checking if the right values were used on each row
        for index, row in predictor.test_data.iterrows():
            color_original_value = original_values[index] # color on the csv file
            if color_original_value == original_values[0]: # checking if it is white
                for created_column_name in created_column_names:
                    # all columns should have a 0 value
                    self.assertEquals(row[created_column_name], 0, 
                                      "Column %s should be populated with a 0" % created_column_name)
            else:
                # it is not white
                
                # getting the column name that should have 1
                col_name = "cat_color_%s" % (color_original_value) 
                for created_column_name in created_column_names:
                    if col_name == created_column_name:
                        self.assertEquals(row[created_column_name], 1, 
                                          "Column %s should be populated with 1" % created_column_name)
                    else:
                        # all other columns should have 0
                        self.assertEquals(row[created_column_name], 0,
                                           "Column %s should be populated with 0" % created_column_name)


    def test_prediction_with_raw_categorical_values(self):
        """
            Checking if an Exception is raised when trying to predict
            without categorical columns preparation
        """
        
        predictor = CSVLogisticPredictor("test_data/simple_example.csv", 
                                     "test_data/simple_example.csv")
        self.assertRaises(PredictionException, predictor.create_logistic_regression_model, 'sold')
    

    def test_invalid_csv_path_raises_error(self):
        """
            An IOError should be raised when an invalid path is passed to the constructor
        """
        self.assertTrue(CSVLogisticPredictor("test_data/example.csv", "test_data/example.csv"))
        self.assertRaises(IOError, CSVLogisticPredictor, "test_data/example.csv", "invalid_path.csv")
        self.assertRaises(IOError, CSVLogisticPredictor, "invalid_path.csv", "test_data/example.csv")


    def test_csv_output_creation(self):
        """
            Checking if a csv file is created when the save_to_csv method is called
        """
        
        # file should not exist yet
        self.assertFalse(os.path.exists(TEST_OUTPUT_FILEPATH), "File should not exist")
        predictor = CSVLogisticPredictor("test_data/simple_example.csv", 
                                     "test_data/simple_example.csv")
        
        predictor.prepare_categorical_columns(['color'])
        
        # An exception should be raised when trying to
        # save a file without a created regression model
        self.assertRaises(PredictionException, predictor.save_to_csv, TEST_OUTPUT_FILEPATH)
        predictor.create_logistic_regression_model('sold')
        del predictor.test_data['sold'] # removing the dependent column
        
        # An exception should be raised when trying to
        # save a file without a prediction
        self.assertRaises(PredictionException, predictor.save_to_csv, TEST_OUTPUT_FILEPATH)
        predictor.predict()
        
        predictor.save_to_csv(TEST_OUTPUT_FILEPATH)
        
        # now the file should be created
        self.assertTrue(os.path.exists(TEST_OUTPUT_FILEPATH))
        
        os.remove(TEST_OUTPUT_FILEPATH)


    def test_inexistent_columns(self):
        """
            Checking if IndexError is raised when passing invalid columns
        """
        self.assertRaises(IndexError, CSVLogisticPredictor, 
                                     "test_data/simple_example.csv", 
                                     "test_data/simple_example.csv",
                                     "inexistent")
        predictor = CSVLogisticPredictor("test_data/simple_example.csv", 
                                     "test_data/simple_example.csv")
        
        self.assertRaises(IndexError, predictor.prepare_categorical_columns, ["inexistent"])
        self.assertRaises(IndexError, predictor.create_logistic_regression_model, "inexistent")

        
if __name__ == '__main__':
    unittest.main()