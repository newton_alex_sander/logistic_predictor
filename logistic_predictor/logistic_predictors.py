import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression

class PredictionException(Exception):
    pass

class LogisticPredictor(object):
    
    def __init__(self, train_dataframe, test_dataframe, index_column=None):
        """The LogisticPredictor uses a train pandas.DataFrame to create
        a logistic prediction model. The prediction model is applied to
        the test pandas.DataFrame, generating a new DataFrame containing 
        the predicted values.
        
        Usage:
            >> logistic_predictor = LogisticPredictor(train_df,
                                                  test_df,
                                                 "index_column")

            >> logistic_predictor.prepare_categorical_columns(['color'])
            
            >> logistic_predictor.create_logistic_regression_model("passed_away")
            >> predicted_df = logistic_predictor.predict()

        Args:
            train_dataframe (pd.DataFrame): Dataframe containing data to 
                create a prediction model.
            test_dataframe (pd.DataFrame): Dataframe containing the data
                that the prediction model will be applied to.
            index_column (Optional[str]): Column label of the index.
        """
        self.train_data = train_dataframe
        self.test_data = test_dataframe
        self.index_column = index_column
        if self.index_column:
            self._apply_index()
        
        self.logistic_model = None
        self.predictions_dataframe = None


    def _apply_index(self):
        """Sets the index column on the train and test DataFrames. The index 
        column is then removed from the DataFrames.
        """
        self._check_for_column_existence(self.index_column, self.train_data, "train")
        
        # setting a customized index for the dataframe
        self.train_data = self.train_data.set_index(self.train_data[self.index_column])
        del self.train_data[self.index_column] # removing the index column
        
        # setting a customized index for the dataframe
        self.test_data = self.test_data.set_index(self.test_data[self.index_column])
        del self.test_data[self.index_column] # removing the index column


    def _check_for_column_existence(self, column_name, data_frame, data_frame_label):
        """Exception raiser to inexistent columns on a dataFrame

        Args:
            column_name(str): The column name to be checked.
            data_frame(pd.DataFrame): The data_frame instance.
            data_frame_label(str): A data_frame label.

        """
        if column_name not in data_frame.columns:
            raise IndexError("Column %s not found on %s dataFrame" % (column_name, data_frame_label))


    def prepare_categorical_columns(self, categorical_columns):
        """To apply the regression function, the columns that have
        categorical values (values that are not binary. Example: black/white/
        red) should be converted to binary (this method creates a new column 
        for each category and removes the first one to prevent multicollinearity

        Args:
            categorical_columns(list): A list of column names.

        Returns:
            created_column_names(list): A list of created columns
        """
        if not isinstance(categorical_columns, list):
            raise TypeError("categorical_columns should be a list of strings")
        created_column_names = []
        for column in categorical_columns:
            self._check_for_column_existence(column, self.train_data, "train")
            self._check_for_column_existence(column, self.test_data, "train")
            
            for i, value in enumerate(self.train_data[column].unique()):
        
                if i == 0: continue # preventing multicollinearity
                
                # creating a new column name
                column_name = "cat_%s_%s" % (column, value.replace(" ",""))
                created_column_names.append(column_name)
                # putting 1 if the column value is equal to the created column
                self.train_data[column_name] = self.train_data[column].map(lambda x: 1 if x == value else 0)
                self.test_data[column_name] = self.test_data[column].map(lambda x: 1 if x == value else 0)
            
            # removing the categorical column
            del self.train_data[column]
            del self.test_data[column]
        return created_column_names


    def create_logistic_regression_model(self, dependent_variable):
        """The dependent variable column is flattened to 1-D array that will be 
        used on the prediction model, the column is then removed from the dataFrame

        Args:
            dependent_variable(string): The label of the dependent variable column.

        Returns:
            a LogisticRegression instance

        """
        self._check_for_column_existence(dependent_variable, self.train_data, "train")
        
        # Flattening to 1-D array
        y = np.ravel(self.train_data[dependent_variable])
        
        # removing dependent variable from the dataframe
        del self.train_data[dependent_variable]
        
        # sklearn.LogisticRegression can not deal with a raw DataFrame data
        try:
            X = self.train_data.to_sparse()
        except TypeError:
            raise PredictionException("Could not sparse your train_data. \
            Check your data, maybe you have categorical columns to create")
        
        model = LogisticRegression()
        logistic_model = model.fit(X, y)
        
        self.logistic_model = logistic_model
        return logistic_model


    def predict(self):
        """Using the created logistic_model to predict the dependent 
        variable on the test dataFrame

        Returns:
            a pd.DataFrame instance containing the prediction values

        """
        if not self.logistic_model:
            raise PredictionException("You should create a regression model before trying to predict")
        
        # predicting test_data values based on the previously created prediction model
        predictions = self.logistic_model.predict_proba(self.test_data.to_sparse())
        predictions_dataframe = pd.DataFrame(predictions, 
                           index=self.test_data.index, 
                           columns=["Probability0", "Probability1"])
        self.predictions_dataframe = predictions_dataframe
        return predictions_dataframe


class CSVLogisticPredictor(LogisticPredictor):

    def __init__(self, csv_train_filepath, csv_test_filepath, index_column=None):
        """The CSVLogisticPredictor is a specialized LogisticPredictor used
        to predict from and/or to csv files.
        Usage:
            >> csv_predictor = CSVLogisticPredictor("data/eBayiPadTrain.csv", 
                                     "data/eBayiPadTest.csv", 
                                     "UniqueID")

            >> categorical_columns = ['condition', 'carrier', 'color', 
                                        'storage', 'productline', 'cellular']
            >> csv_predictor.prepare_categorical_columns(categorical_columns)
            
            >> csv_predictor.create_logistic_regression_model("sold")
            >> csv_predictor.predict()
            
            >> csv_predictor.save_to_csv("data/result.csv", columns=["Probability"])

        Note:
            If index_column is not passed, the first column of the csv file will be used
        
        Args:
            csv_train_filepath (string): The filepath of the csv train file.
            csv_test_filepath (string): The filepath of the csv test file.
            index_column (Optional[str]): Column label of the index

        """
        train_df = pd.DataFrame.from_csv(csv_train_filepath)
        test_df = pd.DataFrame.from_csv(csv_test_filepath)
        super(CSVLogisticPredictor, self).__init__(train_df, test_df, index_column)


    def save_to_csv(self, csv_path, columns=None):
        """Saves the predicted dataFrame to a csv file.

        Args:
            csv_path (string): The filepath of the csv output file.
            columns (Optional[list]): List of column labels that should be 
                persisted on the csv file.

        """
        try:
            self.predictions_dataframe.to_csv(csv_path, columns=columns)
        except AttributeError:
            raise PredictionException("No prediction made, you should run \
            predict() before trying to save the prediction")
        

