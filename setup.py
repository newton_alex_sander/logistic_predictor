from setuptools import setup

setup(
    name = "logistic_predictor",
    version = "0.1",
    author = "Newton Sander",
    author_email = "sandernewtonsander@gmail.com",
    description = ("A logistic predictor that uses pandas and sklearn"),
    install_requires = ["pandas==0.17.1",
                        "numpy==1.10.4",
                        "scikit-learn==0.17"],
    license = "BSD",
    keywords = "sklearn logistic regression",
    packages=['logistic_predictor'],
)