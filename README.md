# README #

This module can be used to predict variables using a logistic regression function

### Installing and using it ###

python setup.py install

```
#!python


from logistic_predictor import CSVLogisticPredictor


csv_predictor = CSVLogisticPredictor("data/train.csv", 
                                     "data/test.csv", 
                                     "UniqueID")

categorical_columns = ['condition', 'carrier', 'color', 'storage', 'productline', 'cellular']
csv_predictor.prepare_categorical_columns(categorical_columns)

csv_predictor.create_logistic_regression_model(dependent_variable="sold")
csv_predictor.predict()

csv_predictor.save_to_csv("data/result.csv", columns=["Probability1"])


```